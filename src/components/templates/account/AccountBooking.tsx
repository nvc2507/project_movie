import { Chair } from "types";

export const AccountBooking = () => {
  const movieChoosedJsonString = localStorage.getItem("movieChoosed");
  if (movieChoosedJsonString)
    var movieInfo = JSON.parse(movieChoosedJsonString);
    
  const localSeatedJsonString = localStorage.getItem("localSeated");
  if (localSeatedJsonString)
    var chooseSeated = JSON.parse(localSeatedJsonString);
  return (
    <div className="text-black mb-14  text-start mt-20 ml-5">
      <div className="border-b-2 pb-4">
        <h1 className="text-2xl font-bold mb-5 text-zinc-500 ">Thông tin phim bạn đã chọn</h1>
        <div className="flex flex-col gap-2 font-bold">
          <p>
            Cụm rạp: <span>{movieInfo?.tenCumRap}</span>
          </p>
          <p>
            Địa chỉ:<span>{movieInfo?.diaChi}</span>
          </p>
          <p>
            Rạp: <span>{movieInfo?.tenRap}</span>
          </p>
          <p>
            Ngày giờ chiếu:<span>{movieInfo?.ngayChieu}~</span>
            {movieInfo?.gioChieu}
          </p>
          <p>
            Tên phim: <span>{movieInfo?.tenPhim}</span>
          </p>
        </div>
      </div>
      <div>
        <h1 className="text-2xl font-bold my-5 text-zinc-500 ">Danh sách ghế bạn đã chọn</h1>
      </div>
      <table className="table-fixed  w-4/5 mt-5 text-center">
        <thead className="border border-slate-400 ">
          <tr>
            <th className="border border-slate-400 ">Các Ghế bạn đã chọn</th>
            <th className="border border-slate-400 ">Giá</th>
          </tr>
        </thead>
        <tbody className="border border-slate-400 ">
          {chooseSeated?.map((seat: Chair) => (
            <tr key={seat.maGhe} className="border border-slate-400 ">
              <td className="border border-slate-400 ">{seat.tenGhe}</td>
              <td className="border border-slate-400 ">{seat.giaVe} vnđ</td>
            </tr>
          ))}
          <tr className="border border-slate-400 ">
            <td className="border border-slate-400 ">Tổng tiền</td>
            <td className="border border-slate-400 ">
              {chooseSeated.reduce(
                (total: number, seat: Chair) => (total += seat.giaVe),
                0
              )}
              {} vnđ
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default AccountBooking;
