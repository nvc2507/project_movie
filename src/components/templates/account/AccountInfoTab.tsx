import { Button, Input } from 'components/ui'
import { useAuth } from 'hooks'
import { useEffect } from 'react'
import { useForm, SubmitHandler } from 'react-hook-form'
import { AccountSchemaType } from 'schema'
import { useAppDispatch } from 'store'
import { updateUserThunk } from 'store/quanLyNguoiDung/thunk'
import { styled } from 'styled-components'
import { toast } from 'react-toastify'

const AccountInfoTab = () => {
    const { user, isLoading } = useAuth();
    const dispatch = useAppDispatch();
    const { reset, register, handleSubmit } = useForm()

    useEffect(() => {
        reset(user)
    }, [user, reset])

    const onSubmit: SubmitHandler<AccountSchemaType> = (value) => { 
        dispatch(updateUserThunk(value)).unwrap().then(() => { 
            toast.success('Cập nhật thông tin thành công')
        });
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="px-40 ">
            <InputS label="Tài khoản" name="taiKhoan" register={register} />
            <InputS label="Họ và tên" name="hoTen" register={register} />
            <InputS label="Số điện thoại" name="soDT" register={register} />
            <InputS label="Email" name="email" register={register} />
            <InputS label="Mã nhóm" name="maNhom" register={register} />
            <InputS disable label="Loại người dùng" name="maLoaiNguoiDung" register={register} />
            <div className="text-right">
                <Button loading={isLoading} htmlType='submit' className="mt-[60px] w-[200px] !h-[50px]" type="primary">
                    Lưu thay đổi
                </Button>
            </div>
        </form>
    )
}

export default AccountInfoTab

const InputS = styled(Input)`
    margin-top: 20px;
    input {
        background-color: transparent !important;
        border: 1px solid black;
        color: black;
    }
`
