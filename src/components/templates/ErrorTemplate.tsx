import { Button } from "components/ui"
import { PATH } from "constant";
import { useNavigate } from "react-router-dom"

export const ErrorTemplate = () => {
  const navigate = useNavigate();  

  return (
    <div className="mt-[100px] h-[350px]">
        <div className="flex flex-col items-center justify-center">
            <h2 className="text-[100px] font-700 text-blue-800">Oops</h2>
            <p className="text-[30px] font-700 mb-2">404 - PAGE NOT FOUND</p>
            <p>The page you are looking for might have been removed</p>
            <p>had its name changed or is temporarily unavailable.</p>
            <Button className="mt-6" size='large' shape="round" type="primary" onClick={() => { navigate(PATH.home) }}>Go to Homepage</Button>
        </div>
    </div>
  )
}

export default ErrorTemplate