import { PATH } from "constant";
import { useState } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import { RootState } from "store";

export const Detail = () => {
  let path = "";
  const navigate = useNavigate();
  const { movieTheaterList, rapList } = useSelector(
    (state: RootState) => state.quanLyRap
  );
  const [heThongRap, setHeThongRap] = useState<string>(
    movieTheaterList[0]?.maHeThongRap
  );


  return (
    <div>
        <h2 className="font-serif text-center text-transparent pt-10 bg-clip-text text-3xl bg-gradient-to-r to-zinc-800 from-zinc-600">
          THÔNG TIN RẠP 
        </h2>
        <div className="grid  lg:grid-cols-12 lg:grid-flow-col gap-4 mt-6 mb-[100px] rounded border border-slate-300 lg:p-10 ">
          <div className="lg:col-span-2 lg: ml-5">
            {movieTheaterList.map((movieTheater) => (
              <div
                className=" flex justify-items-center items-center rounded border border-slate-300 p-10 mb-3 bg-[#f5f5f5]"
                key={movieTheater.maHeThongRap}
                style={{ width: 150, height: 100 }}
              >
                <img
                  style={{
                    height: 70,
                    width: 70,
                  }}
                  src={movieTheater.logo}
                  alt="..."
                  className="cursor-pointer"
                  onClick={() => {
                    setHeThongRap(movieTheater.maHeThongRap);
                  }}
                />
              </div>
            ))}
          </div>
          <div className="col-span-4   overflow-y-scroll h-[660px] sm:w-full">
            {rapList.map((rap) => {
              return (
                <div key={rap.maHeThongRap} className="">
                  {rap.lstCumRap.slice(0, 8).map((cumRap) => {
                    if (rap.maHeThongRap == (heThongRap || "BHDStar")) {
                      return (
                        <div
                          key={cumRap.maCumRap}
                          className="lg:flex rounded border  border-slate-300 ml-3 mb-3 bg-[#f5f5f5]"
                        >
                          <img
                            style={{
                              height: 100,
                              width: 100,
                            }}
                            src={cumRap.hinhAnh}
                            alt=""
                            className="rounded "
                          />
                          <div className="ml-2">
                            <p className="font-500">{cumRap.tenCumRap}</p>
                            <p className="text-12">{cumRap.diaChi}</p>
                          </div>
                        </div>
                      );
                    }
                  })}
                </div>
              );
            })}
          </div>
          <div className="col-span-6  overflow-y-scroll h-[660px]">
            {rapList.map((rap) => {
              return (
                <div key={rap.maHeThongRap}>
                  {rap.lstCumRap.slice(0, 8).map((cumRap) => {
                    if (rap.maHeThongRap == (heThongRap || "BHDStar")) {
                      return (
                        <div key={cumRap.maCumRap}>
                          {cumRap.danhSachPhim.map((phim) => {
                            return (
                              <div
                                key={phim.maPhim}
                                className="  mb-4 h-[160px] rounded border border-slate-300 bg-[#f5f5f5]"
                              >
                                <div className="flex ">
                                  <img
                                    className="rounded"
                                    style={{
                                      height: 100,
                                      width: 100,
                                      marginRight: 15,
                                    }}
                                    src={phim.hinhAnh}
                                    alt="..."
                                  />
                                  <div className="flex">
                                    {phim.hot && (
                                      <span className="bg-red-500 mr-1 h-6 text-white">
                                        HOT
                                      </span>
                                    )}
                                    <h2 className="font-bold cursor-pointer" onClick={() => { navigate(generatePath(PATH.detailmovie, {movieId: phim.maPhim})) }}>{phim.tenPhim}</h2>
                                  </div>
                                </div>
                                <div className="flex font-bold pl-3">
                                  Lịch chiếu :
                                  {phim.lstLichChieuTheoPhim.map(
                                    (lichChieu, index: number) => {
                                      if (index < 2) {
                                        return (
                                          <div key={index} className="">
                                            <button
                                              type="button"
                                              className=""
                                              onClick={() => {
                                                path = generatePath(PATH.purchase, {
                                                  maLichChieu: lichChieu.maLichChieu,
                                                });
                                                navigate(path);
                                              }}
                                            >
                                              <p className="font-bold ml-3">
                                                {" "}
                                                {lichChieu.ngayChieuGioChieu.substring(
                                                  0,
                                                  10
                                                )}
                                              </p>
                                              <p className="font-bold ml-3">
                                                {" "}
                                                {lichChieu.ngayChieuGioChieu.substring(
                                                  11,
                                                  16
                                                )}
                                              </p>
                                            </button>
                                          </div>
                                        );
                                      }
                                    }
                                  )}
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      );
                    }
                  })}
                </div>
              );
            })}
          </div>
        </div>
    </div>
  );
};

export default Detail;
