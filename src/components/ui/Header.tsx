import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { useDispatch } from "react-redux";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";
import { useAuth } from "hooks";
import { useState } from "react";

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { user } = useAuth();
  const accessTokenJson = localStorage.getItem("accessToken");
  if (accessTokenJson) {
    var accessToken = JSON.parse(accessTokenJson);
  }

  const [isMenuVisible, setMenuVisible] = useState(false);

  const toggleMenu = () => {
    setMenuVisible(!isMenuVisible);
  };
  return (
    <HeaderS>
      <div className="header-content">
        <h2
          className="font-600 text-[30px] cursor-pointer uppercase  hover:text-pink-500 rounded-lg transition-all duration-300 "
          onClick={() => {
            navigate("/");
          }}
        >
          Cinephiles Paradise
        </h2>
        <div className="md:hidden pr-5">
          <button onClick={toggleMenu} className=" text-gray-300 hover:text-white focus:outline-none">
            <svg className="h-7 w-7 font-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16m-7 6h7"></path>
            </svg>
          </button>
        </div>
        <div className={`md:flex menu__toggle items-center ${isMenuVisible ? 'block' : 'hidden'}`}>
            <NavLink
              className="links mr-40 font-600  hover:text-pink-500 rounded-lg transition-all duration-300"
              to={PATH.home}
            >
              Home
            </NavLink>
            <NavLink
              className=" links mr-40 font-600  hover:text-pink-500 rounded-lg transition-all duration-300"
              to={PATH.purchase}
            >
              Purchase
            </NavLink>
            <NavLink
              className="links font-600  hover:text-pink-500 rounded-lg transition-all duration-300"
              to={PATH.contact}
            >
              Contact
            </NavLink>
          <div className="popover">
            {accessToken && (
              <Popover
                content={
                  <div className="p-2">
                    <h2 className="font-600 mb-4 p-2">{user?.hoTen}</h2>
                    <hr />
                    <div
                      onClick={() => {
                        navigate(PATH.account);
                      }}
                      className="p-2 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                    >
                      Thông tin tài khoản
                    </div>
                    <div
                      className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                      onClick={() => {
            
                        dispatch(quanLyNguoiDungActions.logOut());
                        navigate(PATH.login);
                      }}
                    >
                      Đăng xuất
                    </div>
                  </div>
                }
                trigger={"click"}
              >
                <Avatar
                  className="avatar !ml-40 hover:cursor-pointer !flex !items-center !justify-center"
                  size={30}
                  icon={<UserOutlined></UserOutlined>}
                ></Avatar>
              </Popover>
            )}
            <div className="user">
              {!accessToken && (
                <NavLink className="user__menu ml-40  font-600  hover:text-pink-500 rounded-lg transition-all duration-300" to={PATH.login}>
                  Login
                </NavLink>
              )}
              {!accessToken && (
                <NavLink className="user__menu ml-40  font-600  hover:text-pink-500 rounded-lg transition-all duration-300" to={PATH.register}>
                  Register
                </NavLink>
              )}
            </div>
          </div>
        </div>
        
      </div>
    </HeaderS>
  );
};

export default Header;

const HeaderS = styled.header`
  height: var(--header-height);

  background: #71717a;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1;
  .header-content {
    display: fixed;
    color: white;
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;


    @media screen and (max-width: 768px) {
      .links {
        display: block;
        margin:5px 0;
        text-align: center;
      }
      .user {
        display: block;

        .user__menu {
          display: block;
          margin: 0;
          text-align: center;
        }
      }

      .popover {
        display: flex;
        margin: 0;
        align-items: center;
        justify-content: center;
      }
      .avatar {
        margin: 10px 0px !important;
      }
      .menu__toggle {
        position: absolute;
        top: 80px;
        right:0px;
        background-color: #71717a;
        width: 200px;
        height: 400px;
        align-items: center;
      }
    }
  }
`;
