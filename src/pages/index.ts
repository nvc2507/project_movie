export * from './Login';
export * from './Register';
export * from './Home';
export * from './Account';
export * from './DetailMovie';
export * from './Purchase';
export * from './Error';
