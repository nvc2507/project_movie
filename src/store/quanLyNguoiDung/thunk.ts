import { createAsyncThunk } from "@reduxjs/toolkit";
import { AccountSchemaType, LoginSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";

export const loginThunk = createAsyncThunk('quanLyNguoiDung/loginThunk', async (payload: LoginSchemaType, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.login(payload);
        return data.data.content;
    } catch (error) {
        return rejectWithValue(error);
    }
});

export const getUserThunk = createAsyncThunk('quanLyNguoiDung/getUserThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyNguoiDungServices.getUser();
        return data.data.content;
    } catch (error) {
        return rejectWithValue(error)
    }
});

export const updateUserThunk = createAsyncThunk('quanLyNguoiDung/updateUserThunk',async (payload: AccountSchemaType, {rejectWithValue}) => {
    try {
        await quanLyNguoiDungServices.updateUser(payload)

        await new Promise(resolve => setTimeout(resolve, 2000))
    } catch (error) {
        return rejectWithValue(error);
    }
})
