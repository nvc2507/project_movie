import { createSlice } from '@reduxjs/toolkit'
import { getUserThunk, loginThunk, updateUserThunk } from './thunk';
import { User, UserInfo } from 'types';


type QuanLyNguoiDungState = {
    user?: User | UserInfo;
    accessToken?: string;
    isLoading?: boolean;
}

const initialState: QuanLyNguoiDungState = {
    user: undefined,
    accessToken: JSON.parse(localStorage.getItem('accessToken')),
    isLoading: false
}

const quanLyNguoiDungSlice = createSlice({
    name: 'quanLyNguoiDung',
    initialState,
    reducers: {
        logOut: (state) => {
            state.user = undefined;
            state.isLoading = false;
            state.accessToken = undefined;
            localStorage.removeItem('accessToken');
        }
    }, // xử lý đồng bộ
    extraReducers: (builder) => { 
        builder
            .addCase(loginThunk.fulfilled, (state, { payload }) => {
                state.user = payload;
                if(payload){
                    localStorage.setItem('accessToken',JSON.stringify(payload.accessToken));
                }
            })
            .addCase(getUserThunk.fulfilled, (state, { payload }) => {
                if(payload){
                    state.user = payload;
                }
            })
            .addCase(updateUserThunk.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(updateUserThunk.fulfilled, (state) => {
               state.isLoading = false;
            })
            .addCase(updateUserThunk.rejected, (state) => {
                state.isLoading = false;
            })
    }, // xử lý bđb, api
})

export const { reducer: quanLyNguoiDungReducer, actions: quanLyNguoiDungActions } = quanLyNguoiDungSlice;