import { createSlice } from "@reduxjs/toolkit"
import { HeThong, MovieTheater, ThongTinChiTiet } from "types"
import { getMovieTheaterListThunk, layThongTinLichChieuHeThongRapThunk, layThongTinLichChieuPhimThunk } from "./thunk"

type MovieTheaterState = {
    movieTheaterList: MovieTheater[],
    rapList: HeThong[],
    thongTinLichChieu: ThongTinChiTiet,
}

const initialState: MovieTheaterState = {
    movieTheaterList: [],
    rapList: [],
    thongTinLichChieu: undefined,
}

const quanLyRapSlice = createSlice({
    name: 'quanLyRap',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getMovieTheaterListThunk.fulfilled, (state, {payload}) => {
                state.movieTheaterList = payload;
            })
            .addCase(layThongTinLichChieuHeThongRapThunk.fulfilled, (state, {payload}) => {
                state.rapList = payload;
            })
            .addCase(layThongTinLichChieuPhimThunk.fulfilled, (state, {payload}) => {
                state.thongTinLichChieu = payload;
            })
    } 
})

export const { reducer: quanLyRapReducer, actions: quanLyRapActions } = quanLyRapSlice