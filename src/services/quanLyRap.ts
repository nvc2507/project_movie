import { apiInstance } from "constant";
import { HeThong, MovieTheater, ThongTinChiTiet } from "types";

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_RAP,
})

export const quanLyRapServices = {
    getDetailMovieTheater: () => api.get<ApiResponses<MovieTheater[]>>('/LayThongTinHeThongRap'),
    layThongTinLichChieuHeThongRap: () => api.get<ApiResponses<HeThong[]>>('/LayThongTinLichChieuHeThongRap'),
    layThongTinLichChieuPhim: (payload: string) => api.get<ApiResponses<ThongTinChiTiet>>(`/LayThongTinLichChieuPhim?MaPhim=${payload}`)
}