import { apiInstance } from "constant"
import { AccountSchemaType, RegisterSchemaType } from "schema"
import { LoginSchemaType } from "schema/LoginSchema"
import { User, UserInfo } from "types/quanLyNguoiDung"


const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_DANH_SACH_NGUOI_DUNG,
})

export const quanLyNguoiDungServices = {
    register: (payload: RegisterSchemaType) => api.post('/DangKy', payload),
    login: (payload: LoginSchemaType) => api.post<ApiResponses<User>>('/DangNhap', payload),
    getUser: () => api.post<ApiResponses<UserInfo>>('/ThongTinTaiKhoan'),
    updateUser: (payload: AccountSchemaType) => api.put('/CapNhatThongTinNguoiDung', payload),
}