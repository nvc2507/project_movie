export type Chair = {
  daDat: boolean;
  giaVe: number;
  loaiGhe: string;
  maGhe: number;
  maRap: number;
  stt: string;
  taiKhoanNguoiDat: string;
  tenGhe: string;
};
