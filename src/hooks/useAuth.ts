import { useSelector } from "react-redux"
import { RootState } from "store"

export const useAuth = () => {
    const { user, accessToken, isLoading } = useSelector((state: RootState) => state.quanLyNguoiDung);
    return { user, accessToken, isLoading };
}